const SERIAL_PORT = '/dev/ttyACM0'

const restify = require('restify')
const socketIO = require('socket.io')
const SerialPort = require('serialport')
const delimiter = new SerialPort.parsers.Readline({delimiter: "\n"})

const server = restify.createServer()
const io = socketIO(server.server)

const connections = {}

let port = new SerialPort(SERIAL_PORT, { baudRate: 9600 })

port
    .pipe(delimiter)
    .on('data', buffer => {
        try {
            Object
                .values(connections)
                .forEach(socket => socket.emit('stats', JSON.parse(buffer)))
        } catch(err) {
            console.error(err)
        }
    })

port.on('error', console.error)

io.on('connection', socket => {
    connections[socket.id] = socket

    socket.on('action', ({action}) => {
        if(action) {
            port.write(action)
        }
    })

    socket.on('disconnect', () => delete connections[socket.id])
})

server.listen(3000, '0.0.0.0', () => console.log("Server started"))