#include <LiquidCrystal.h>
#include <DHT.h>
#include <IRremote.h>

#define DEGREE_CHAR ((char) 223)
#define TEMP_SENSOR_PIN A8
#define LUM_SENSOR_PIN A9
#define IR_PIN 42
#define RELE_FAN_PIN 38
#define RELE_LAMP_PIN 40

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);
DHT dht(TEMP_SENSOR_PIN, DHT11);
IRrecv irrecv(IR_PIN);
decode_results results;
unsigned long buttonKey = 0;

char menu = 'T';

float temperature;
int luminosity;

int temperatureLimit = 30;
int luminosityLimit = 60;

boolean isFanOn = false;
boolean isLampOn = false;
boolean mustBeFanOn = true;
boolean mustBeLampOn = true;
boolean isFanAutomatic = true;
boolean isLampAutomatic = true;

byte lua[] = {
    B00111,
    B01110,
    B11100,
    B11100,
    B11100,
    B11100,
    B01110,
    B00111
};

byte sol1[] = {
    B01001,
    B00100,
    B10001,
    B00011,
    B01011,
    B10001,
    B00100,
    B01001
};

byte sol2[] = {
    B10010,
    B00100,
    B10001,
    B11010,
    B11000,
    B10001,
    B00100,
    B10010
};

void setup() {
    Serial.begin(9600);
    lcd.begin(16, 2);
    dht.begin();

    pinMode(TEMP_SENSOR_PIN, INPUT ) ;
    pinMode(RELE_FAN_PIN, OUTPUT);
    pinMode(RELE_LAMP_PIN, OUTPUT);

    lcd.createChar(0, lua);
    lcd.createChar(1, sol1);
    lcd.createChar(2, sol2);

    irrecv.enableIRIn();
    irrecv.blink13(true);
}

void loop() {
    temperature = dht.readTemperature();
    luminosity = analogRead ( LUM_SENSOR_PIN );

    showMenu();
    performIRRemoteAction();
    performSerialInstructions();

    traitTemperature();
    traitLuminosity();

    delay(500);
}

void traitTemperature() {
    if(isFanAutomatic) {
        isFanOn = temperature >= temperatureLimit;
    } else {
        isFanOn = mustBeFanOn;
    }

    if(isFanOn) {
        digitalWrite(RELE_FAN_PIN, HIGH);
    } else {
        digitalWrite(RELE_FAN_PIN, LOW);
    }
}

void traitLuminosity() {
    if(isLampAutomatic) {
        isLampOn = luminosity <= luminosityLimit;
    } else {
        isLampOn = mustBeLampOn;
    }
    
    if(isLampOn) {
        digitalWrite(RELE_LAMP_PIN, HIGH);
    } else {
        digitalWrite(RELE_LAMP_PIN, LOW);
    }
}

void performIRRemoteAction() {
    if (irrecv.decode(&results)){
        
        if (results.value == 0XFFFFFFFF) {
            results.value = buttonKey;
        }

        buttonKey = results.value;

        switch(buttonKey) {
            case 0xFF38C7: // ok
                switchAutomaticMode();
                break;
            case 0xFF5AA5: // right
                menu = 'L';
                lcd.clear();
                break;
            case 0xFF10EF: // left
                menu = 'T';
                lcd.clear();
                break;
            case 0xFF18E7: // up
                if (menu == 'T')
                    temperatureLimit++;
                else
                    luminosityLimit += 10;

                break;
            case 0xFF4AB5: // down
                if (menu == 'T')
                    temperatureLimit--;
                else
                    luminosityLimit -= 10;

                break;
        }

        irrecv.resume();
    }
}

void switchAutomaticMode() {
    isFanAutomatic = !isFanAutomatic;
    isLampAutomatic = !isLampAutomatic;
    mustBeFanOn = !isFanAutomatic;
    mustBeLampOn = !isLampAutomatic;
}

void performSerialInstructions() {
    if (Serial.available() > 0) {
        char serialOperation = Serial.read();

        switch(serialOperation) {
            case '1':
                mustBeLampOn = !mustBeLampOn;
                break;
            case '2':
                mustBeFanOn = !mustBeFanOn;
                break;
            case '3':
                switchAutomaticMode();
                break;
        }
    }

    Serial.println(
        "{\"temp\": " + String((int) temperature) + 
        ",\"lumi\": " + String((int) luminosity) + 
        ",\"isFanOn\": " + String(isFanOn) + 
        ",\"isLampOn\": " + String(isLampOn) + 
        ",\"mustBeFanOn\": " + String(mustBeFanOn) + 
        ",\"mustBeLampOn\": " + String(mustBeLampOn) + 
        ",\"isFanAutomatic\": " + String(isFanAutomatic) + 
        ",\"isLampAutomatic\": " + String(isLampAutomatic) + 
    "}");
}

void showMenu() {
    if (menu == 'T') {
        showTemperatureMenu();
    } else {
        showLuminostyMenu();
    }
}

void showTemperatureMenu() {
    lcd.setCursor(0, 0);
    lcd.write("Temperatura: ");
    writeTemp((int) temperature);

    lcd.setCursor(0, 1);
    lcd.write("Limite: ");
    writeTemp(temperatureLimit);
}

void showLuminostyMenu() {
    lcd.setCursor(0, 0);
    lcd.write("Lumin: ");
    lcd.print(luminosity);
    lcd.write(" ");

    if(luminosity >= luminosityLimit) {
        lcd.write(byte(1));
        lcd.write(byte(2));
    } else {
        lcd.write(byte(0));
        lcd.write(" ");
    }

    lcd.setCursor(0, 1);
    lcd.write("Limite: ");
    lcd.print(luminosityLimit);
}

void writeTemp(int temp) {
    lcd.print(String(temp));
    lcd.write(DEGREE_CHAR);
    lcd.write("C");
}
