import io from 'socket.io-client'

const checkButtonActive = (button, isActive) => {
    if(isActive) {
        if(!button.classList.contains('active')) 
            button.classList.add('active')
    } else if(button.classList.contains('active')) {
        button.classList.remove('active')
    }
}

window.onload = () => {
    const socket = io('http://localhost:3000')
    
    const speechKeywords = [
        {action: '1', keys: ['lampada', 'luz', 'claridade', 'lâmpada']},
        {action: '2', keys: ['vento', 'fan', 'fã', 'ventilador', 'cooler']},
        {action: '3', keys: ['automatico', 'automático', 'automação', 'livre', 'dispensar', 'auto']},
    ]

    const traitSpeech = result => {
        const lookingFor = result.toLowerCase()
        speechKeywords
            .filter(({keys}) => keys.find(key => lookingFor.includes(key.toLocaleLowerCase())))
            .forEach(({action}) => sendAction(action))
    }

    window.listenMicrophone = event => {
        if (!webkitSpeechRecognition) return;

        const micButton = event.srcElement
        const recognition = new webkitSpeechRecognition()

        micButton.classList.add('active')
                
        recognition.onresult = event => {
            const results = Array
                .from(
                    event.results,
                    res => {
                        const result = Array
                            .from(res)
                            .map(resp => resp.transcript)
                            .join(" ")

                        return result
                    }
                )

            console.log(results)

            traitSpeech(results.join(" "))
        }

        recognition.onend = () => document
            .getElementById('mic')
            .classList
            .remove('active')

        recognition.start()
    }

    window.sendAction = action => {
        socket.emit('action', {action})
    }
    
    const tempDisplay = document.querySelector('#temp')
    const lumitDisplay = document.querySelector('#lumi')

    socket.on('stats', ({temp, lumi, isFanOn, isLampOn, isLampAutomatic, isFanAutomatic}) => {
        const lampButton = document.querySelector('#lamp')
        const fanButton = document.querySelector('#fan')
        const automaticButton = document.querySelector('#automatic')

        tempDisplay.textContent = `${temp}°`
        lumitDisplay.textContent = lumi

        checkButtonActive(lampButton, isLampOn)
        checkButtonActive(fanButton, isFanOn)
        checkButtonActive(automaticButton, isLampAutomatic || isFanAutomatic)
    })
}